@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                          <h1>  {{ session('status') }}</h1>
                        </div>
                    @endif

                   <h1> {{ __('You are logged in!') }}</h1>
                </div>
                <div class="d-flex justify-content-center my-5">
                    <a href="/subjects/create"><h1>Create Subject </h1></a>
                </div>    

            </div>
        </div>
    </div>
</div>
@endsection
