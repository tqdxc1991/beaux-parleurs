@extends('layouts.app')

@section('content')
        <div class="d-flex justify-content-center">
          <div class="card text-white bg-primary" style="width:50%;">
                <div class="card-header">Battle ID - {{$battle->id}}</div>
                <div class="card-body">
                    <h5 class="card-title">{{$battle->date}} - {{$battle->place}}</h5>
                    <h5 class="card-text">Subject - <a href="/roulette"><span class="badge bg-light text-dark">&#128512;Roulette&#128512;</span></a> </h5>
                    <p>Performers:</p>
                    
                  
                    <ul>
                    @foreach($users as $user)
                    <li>{{$user->name}}</li>
                    @endforeach
                    </ul>
                    
                    @if(count($users)==0)
                    <p>Not chosen yet.</p>
                    @endif
            </div> 
        </div>


@endsection
