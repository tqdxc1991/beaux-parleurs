@extends('layouts.app')

@section('content')

        <main class="container ">
        <div class="d-flex justify-content-center my-5">
            <img  src="/images/logo.png" class="img-fluid" alt="logo de societe">
        </div>
            <h1>Subject </h1>
        
            <div>
            {{ $subject->content }}                  
            @if($subject->status === 1)
            <span style="color:red"> - Performed</span>
            @else
            <span style="color:blue"> - Not performed</span> 
            @endif

            </div>
        </main>
      

@endsection

