@extends('layouts.app')

@section('content')

        <main class="container ">

        <div class="d-flex justify-content-center my-5">
            <h1>Create Subject </h1>
        </div>    
        <div class="d-flex justify-content-center my-5">
        <form action="/subjects" method="POST">
        @csrf
            <div>
                <label for="subject">Enter the subject here: </label>
                <input type="text" name="subject" required>
            </div>

            <div class="d-flex justify-content-center my-5">
                <input type="submit" value="Submit!">
            </div>
        </div>
       </form>
        </main>
     
      
@endsection

