@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-12 col-md-6">
        <div class="d-flex justify-content-center">
            <img  src="/images/logo.png" class="img-fluid" alt="logo de societe">
        </div>
    </div>
    <div class="col-12 col-md-6">
        
            @foreach($battles as $battle)
            <div class="card text-white bg-primary" >
                <div class="card-header">Battle ID - {{$battle->id}}</div>
                <div class="card-body">
                    <h5 class="card-title">{{$battle->date}} - {{$battle->place}}</h5>
                    <h4 class="card-text">Subject - <a href="/roulette"><span class="badge bg-light text-dark">&#128512;Roulette&#128512;</span></a> --> <u><a href="/battles/{{$battle->id}}" class="text-white">See more details</a></u></h4>
            </div>
            @endforeach
        
    </div>
  </div>
        




@endsection
