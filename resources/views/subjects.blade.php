@extends('layouts.app')

@section('content')
  
        <main class="container justify-content-center my-5 ">
 

            <h3 style="color:red">{{ session('msg') }}</h3>
            <h1>List of all subjects </h1>
    
            @foreach($subjects as $subject)
            <div class="d-flex p-2 bd-highlight">
                
                <a href="/subjects/{{$subject->id}} "><button type="button" class="btn btn-info">{{$subject->id}}</button></a> <b>{{ $subject->content }}</b>
                @if($subject->status === 1)
                <span style="color:red"> - Performed</span>
                @else
                <span style="color:blue"> - Not performed</span> 
                @endif
                
             </div>
            @endforeach
 
        </main>


        
        
       
@endsection
