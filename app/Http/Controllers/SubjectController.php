<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Subject;
use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{
    public function index(){
        //choose the subjects that have not been performed
        $subjects = Subject::All();

        return view('subjects',['subjects' => $subjects]);
    }

    public function show($id){
        $subject = Subject::FindOrFail($id);
        return view('subject',['subject' => $subject]);
    }

    public function create(){
        return view('subject_create');
    }

    public function store(){
        $subject = new Subject();
        $subject->content = request('subject');
        $subject->status = 0;
        error_log($subject);
        $subject->save();
        return redirect('/subjects')->with('msg','thanks for submitting!');
    }

    public function update(){
        //get all the subjects unperformed
        $subjects = Subject::where('status',0)->get();
        $last = count($subjects)-1;

        //choose a random subject
        $num = random_int(0,$last);
        $sub_chosen = $subjects[$num];
        $id = $sub_chosen->id;

        //find the subject by id, and update the database
        //Subject::FindOrFail($id)->status = 1;
        DB::update(
            'update subjects set status = 1 where id ='.$id
        );
        
        return  view('roulette',['sub_chosen' => $sub_chosen]);
    }
}
