<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Battle;

class BattleController extends Controller
{
    public function index(){
        $battles = Battle::All();
        return view('welcome',['battles'=>$battles]);
    }

    public function show($id){
        $battle = Battle::FindOrFail($id);
        $users = $battle->users()->orderBy('name')->get();
        return view('battle',['battle' => $battle],['users'=>$users]);
    }
}
