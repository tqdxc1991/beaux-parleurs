<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    use HasFactory;
        /**
     * The users that belong to the battle.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

        /**
     * Get the subject associated with the battle.
     */
    public function subject()
    {
        return $this->hasOne(Subject::class);
    }
}
