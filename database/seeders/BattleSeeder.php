<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Battle;

class BattleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Battle::factory()
        ->count(50)
        ->create();
    }
}
